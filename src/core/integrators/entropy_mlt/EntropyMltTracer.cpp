#include "EntropyMltTracer.hpp"

#include "sampling/UniformSampler.hpp"

namespace Tungsten {

EntropyMltTracer::EntropyMltTracer(TraceableScene *scene, const EntropyMltSettings &settings, uint32 threadId,
        UniformSampler &sampler, ImagePyramid *pyramid)
: TraceBase(scene, settings, threadId),
  _settings(settings),
  _sampler(sampler.state(), threadId*3 + 0),
  _cameraSampler(UniformSampler(sampler.state(), threadId*3 + 1)),
  _emitterSampler(UniformSampler(sampler.state(), threadId*3 + 2)),
  _chains(new MarkovChain[_settings.maxBounces + 1]),
  _lightSplatScale(1.0f/(_scene->cam().resolution().x()*_scene->cam().resolution().y())),
  _pyramid(pyramid)
{
}

void EntropyMltTracer::tracePaths(LightPath & cameraPath, PathSampleGenerator & cameraSampler,
                                      LightPath &emitterPath, PathSampleGenerator &emitterSampler,
                                      int s, int t, Vec2u p)
{
    if (t == -1)
        t = _settings.maxBounces + 1;
    if (s == -1)
        s = _settings.maxBounces;

    cameraPath.clear();
    emitterPath.clear();

    if (t > 0) {
        Vec2f resF(_scene->cam().resolution());
        Vec2u pixel;
        if(p[0] == 10000)
            pixel = min(Vec2u(resF*cameraSampler.next2D()), _scene->cam().resolution());
        else
            pixel = p;
        cameraPath.startCameraPath(&_scene->cam(), pixel);
        cameraPath.tracePath(*_scene, *this,  cameraSampler, t);
    }
    if (s > 0) {
        float lightPdf;
        const Primitive *light = chooseLightAdjoint(emitterSampler, lightPdf);
        emitterPath.startEmitterPath(light, lightPdf);
        emitterPath.tracePath(*_scene, *this, emitterSampler, s);
    }
}

int EntropyMltTracer::evalSample(LightPath & cameraPath, PathSampleGenerator & cameraSampler,
                                     LightPath &emitterPath, PathSampleGenerator &emitterSampler,
                                     int length, SplatQueue &queue)
{
    queue.clear();

    int s = int(emitterSampler.next1D()*(length + 1));
    int t = length + 1 - s;

    tracePaths(cameraPath, cameraSampler, emitterPath, emitterSampler, s, t);

    int cameraLength =  cameraPath.length();
    int  lightLength = emitterPath.length();

    if (cameraLength != t || lightLength != s)
        return s;

    if (s == 0) {
        Vec2u pixel = cameraPath[0].cameraRecord().pixel;
        Vec3f v = cameraPath.bdptWeightedPathEmission(t, t);
        queue.addSplat(s, t, pixel, v);
    } else if (t == 1) {
        Vec2f pixel;
        Vec3f splatWeight;
        if (LightPath::bdptCameraConnect(*this, cameraPath, emitterPath, s, _settings.maxBounces, emitterSampler, splatWeight, pixel))
            queue.addFilteredSplat(s, t, pixel, splatWeight*_lightSplatScale);
    } else {
        Vec2u pixel = cameraPath[0].cameraRecord().pixel;
        Vec3f v = LightPath::bdptConnect(*this, cameraPath, emitterPath, s, t, _settings.maxBounces, cameraSampler);
        queue.addSplat(s, t, pixel, v);
    }

    return s;
}

void EntropyMltTracer::traceCandidatePath(LightPath &cameraPath, LightPath &emitterPath,
        SplatQueue &queue, const std::function<void(Vec3f, int, int)> &addCandidate, Vec2u p)
{
    tracePaths(cameraPath, _cameraSampler, emitterPath, _emitterSampler, -1, -1, p);
    int cameraLength = cameraPath.length();
    int  lightLength = emitterPath.length();
    for (int s = 0; s <= lightLength; ++s) {
        int upperBound = min(_settings.maxBounces - s + 1, cameraLength);
        for (int t = 1; t <= upperBound; ++t) {
            if (!cameraPath[t - 1].connectable() || (s > 0 && !emitterPath[s - 1].connectable()))
                continue;

            if (s == 0) {
                if (t - 2 < _settings.minBounces || t - 2 >= _settings.maxBounces)
                    continue;
                Vec3f v = cameraPath.bdptWeightedPathEmission(t, t);
                queue.addSplat(0, t, cameraPath[0].cameraRecord().pixel, v);
                addCandidate(v, 0, t);
            } else if (t == 1) {
                Vec2f pixel;
                Vec3f splatWeight;
                if (LightPath::bdptCameraConnect(*this, cameraPath, emitterPath, s, _settings.maxBounces, _emitterSampler, splatWeight, pixel)) {
                    queue.addFilteredSplat(s, t, pixel, splatWeight*_lightSplatScale);
                    addCandidate(splatWeight*_lightSplatScale, s, t);
                }
            } else {
                Vec3f v = LightPath::bdptConnect(*this, cameraPath, emitterPath, s, t, _settings.maxBounces, _cameraSampler);
                queue.addSplat(s, t, cameraPath[0].cameraRecord().pixel, v);
                addCandidate(v, s, t);
            }
        }
    }
}
//计算香浓熵
float EntropyMltTracer::GetShannon(std::vector<Vec3f> sv) {
    std::vector<float> temp(256, 0);
    for(int i = 0; i < sv.size(); i++) {
        int j = (int)((sv[i][0]*299 + sv[i][1]*587 + sv[i][2]*114) * 255 / 1000);
        if(j > 255)
            j = 255;
        temp[j] += 1;
    }
    for (int i = 0; i < 256; i++)
    {
        temp[i] = temp[i] / sv.size();
    }
    float result = 0.0f;
    for (int i = 0; i < 256; i++)
    {
        if (temp[i] > 0.0f)
        {
            result -= (temp[i] * (log(temp[i]) / log(2.0)));
        }
    }
    float temp2 = 1.0 / sv.size();
    float maxShannon2 = -(temp2 * (log(temp2) / log(2.0)) * sv.size());
    return  result / maxShannon2;
}

void EntropyMltTracer::startSampleChain(int s, int t, float luminance, UniformSampler &cameraReplaySampler,
        UniformSampler &emitterReplaySampler)
{
    int length = s + t - 1;

    MarkovChain &chain = _chains[length];
    chain.currentSplats.reset(new SplatQueue(1));
    chain.proposedSplats.reset(new SplatQueue(1));
    chain.cameraPath.reset(new LightPath(length + 1));
    chain.emitterPath.reset(new LightPath(length));
    chain.cameraSampler .reset(new MetropolisSampler( &cameraReplaySampler, (length + 1)*16));
    chain.emitterSampler.reset(new MetropolisSampler(&emitterReplaySampler, (length + 1)*16));
    chain.currentS = s;

    chain.emitterSampler->setRandomElement(0, (s + 0.5f)/(length + 1.0f));
    evalSample(*chain.cameraPath, *chain.cameraSampler, *chain.emitterPath, *chain.emitterSampler,
            length, *chain.currentSplats);

    chain.cameraSampler ->accept();
    chain.emitterSampler->accept();
    chain.cameraSampler ->setHelperGenerator(&_sampler);
    chain.emitterSampler->setHelperGenerator(&_sampler);

    // if (chain.currentSplats->totalLuminance() != luminance)
    //    FAIL("Underlying integrator is not consistent. Expected a value of %f, but received %f", luminance, chain.currentSplats->totalLuminance());
}

LargeStepTracker EntropyMltTracer::runSampleChain(int pathLength, int chainLength,
        MultiplexedStats &stats, float luminanceScale, std::vector<Vec3f> &filmTsallis)
{
    MarkovChain &chain = _chains[pathLength];
    MetropolisSampler &cameraSampler = *chain.cameraSampler;
    MetropolisSampler &emitterSampler = *chain.emitterSampler;
    LightPath & cameraPath = *chain. cameraPath;
    LightPath &emitterPath = *chain.emitterPath;
    std::unique_ptr<SplatQueue> & currentSplats = chain.currentSplats;
    std::unique_ptr<SplatQueue> &proposedSplats = chain.proposedSplats;
    int &currentS = chain.currentS;
    LargeStepTracker largeSteps;
    float currentB = 0.0f;
    float accumulatedWeight = 0.0f;
    for (int i = 0; i < chainLength; ++i) {
		//动态更新大突变概率
        float largeStepProbability = std::min(std::max( _settings.largeStepProbability - currentB,  _settings.largeStepProbability / 2),  _settings.largeStepProbability * 2);
        bool largeStep = _sampler.next1D() < largeStepProbability;
        cameraSampler.setLargeStep(largeStep);
        emitterSampler.setLargeStep(largeStep);
        int proposedS = evalSample(cameraPath, cameraSampler, emitterPath, emitterSampler, pathLength, *proposedSplats);
        Vec2u p = cameraPath[0].cameraRecord().pixel;
        std::vector<Vec3f> sv;
        int w = _scene->cam().resolution().x();
        int h = _scene->cam().resolution().y();
        for(int j = 0; j < 9;j++)
        {
            for(int k = 0 ; k < 9; k++)
            {
                Vec2f nRaster;
                Vec2f offset ;
                offset = Vec2f(j - 0.5f - 9 / 2, k - 0.5f - 9 / 2);
                nRaster = Vec2f(p[0] + offset[0], p[1] + offset[1]);
                int x = (uint32)nRaster[0], y = (uint32) nRaster[1];
                if( x < 0 || x >= w || y < 0 || y >= h)
                {

                }
                else
                {
                    sv.push_back(filmTsallis[x * h + y]);
                }
            }
        }
        float proposedB = GetShannon(sv);
		//更新权重为信息熵*亮度
        float currentI = currentSplats->totalLuminance() * currentB;
        float proposedI = proposedSplats->totalLuminance() * proposedB;
        if (std::isnan(proposedI))
            proposedI = 0.0f;

        if (largeStep)
            largeSteps.add(proposedI*(pathLength + 1));

        float a = currentI == 0.0f ? 1.0f : min(proposedI/currentI, 1.0f);

		float currentWeight = (1.0f - a);
        float proposedWeight = a;

        accumulatedWeight += currentWeight;

        if (_sampler.next1D() < a) {
            if (currentI != 0.0f)
                currentSplats->apply(*_scene->cam().splatBuffer(), accumulatedWeight/currentI);
            std::swap(currentSplats, proposedSplats);
            accumulatedWeight = proposedWeight;
            currentB = proposedB;
            cameraSampler.accept();
            emitterSampler.accept();

            if (largeStep)
                stats.largeStep().accept(pathLength);
            else if (currentS != proposedS)
                stats.techniqueChange().accept(pathLength);
            else
                stats.smallStep().accept(pathLength);

            currentS = proposedS;
        } else {
            if (proposedI != 0.0f)
                proposedSplats->apply(*_scene->cam().splatBuffer(), proposedWeight/proposedI);
            cameraSampler.reject();
            emitterSampler.reject();

            if (largeStep)
                stats.largeStep().reject(pathLength);
            else if (currentS != proposedS)
                stats.techniqueChange().reject(pathLength);
            else
                stats.smallStep().reject(pathLength);
        }

        if (_pyramid)
            currentSplats->apply(*_pyramid, luminanceScale/currentSplats->totalLuminance());
    }

    if (currentSplats->totalLuminance() != 0.0f)
        currentSplats->apply(*_scene->cam().splatBuffer(), accumulatedWeight/currentSplats->totalLuminance());

    return largeSteps;
}

}
