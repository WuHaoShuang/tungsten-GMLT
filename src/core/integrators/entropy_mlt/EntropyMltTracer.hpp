#ifndef EntropyMLTTRACER_HPP_
#define EntropyMLTTRACER_HPP_

#include "EntropyMltSettings.hpp"
#include "integrators/multiplexed_mlt/MultiplexedStats.hpp"
#include "integrators/multiplexed_mlt/LargeStepTracker.hpp"

#include "integrators/bidirectional_path_tracer/ImagePyramid.hpp"
#include "integrators/bidirectional_path_tracer/LightPath.hpp"

#include "integrators/kelemen_mlt/MetropolisSampler.hpp"
#include "integrators/kelemen_mlt/SplatQueue.hpp"

#include "integrators/TraceBase.hpp"

#include "sampling/UniformPathSampler.hpp"

namespace Tungsten {

class AtomicFramebuffer;

class EntropyMltTracer : public TraceBase
{
private:
    struct MarkovChain
    {
        std::unique_ptr<MetropolisSampler>  cameraSampler;
        std::unique_ptr<MetropolisSampler> emitterSampler;
        std::unique_ptr<LightPath>  cameraPath;
        std::unique_ptr<LightPath> emitterPath;
        std::unique_ptr<SplatQueue>  currentSplats;
        std::unique_ptr<SplatQueue> proposedSplats;
        int currentS;
    };

    EntropyMltSettings _settings;
    UniformSampler _sampler;
    UniformPathSampler _cameraSampler;
    UniformPathSampler _emitterSampler;
    std::unique_ptr<MarkovChain[]> _chains;
    float _lightSplatScale;
    std::vector<float[3]> filmTsallis;
    ImagePyramid *_pyramid;

    void tracePaths(LightPath & cameraPath, PathSampleGenerator & cameraSampler,
                    LightPath &emitterPath, PathSampleGenerator &emitterSampler,
                    int s = -1, int t = -1, Vec2u point = Vec2u(10000, 10000));

    int evalSample(LightPath & cameraPath, PathSampleGenerator & cameraSampler,
                   LightPath &emitterPath, PathSampleGenerator &emitterSampler,
                   int length, SplatQueue &queue);

public:
    EntropyMltTracer(TraceableScene *scene, const EntropyMltSettings &settings, uint32 threadId,
            UniformSampler &sampler, ImagePyramid *pyramid);

    void traceCandidatePath(LightPath &cameraPath, LightPath &emitterPath,
            SplatQueue &queue, const std::function<void(Vec3f, int, int)> &addCandidate, Vec2u p = Vec2u(10000, 10000));
    float GetShannon(std::vector<Vec3f>);
    void startSampleChain(int s, int t, float luminance, UniformSampler &cameraReplaySampler,
            UniformSampler &emitterReplaySampler);
    LargeStepTracker runSampleChain(int pathLength, int chainLength, MultiplexedStats &stats, float luminanceScale, std::vector<Vec3f> &filmTsallis);

    UniformPathSampler &cameraSampler()
    {
        return _cameraSampler;
    }

    UniformPathSampler &emitterSampler()
    {
        return _emitterSampler;
    }
};

}

#endif /* EntropyMLTTRACER_HPP_ */
